﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Paythem AESCTR")]
[assembly: AssemblyDescription("AES CTR encryption system")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Paythem.Net™")]
[assembly: AssemblyProduct("Paythem AESCTR")]
[assembly: AssemblyCopyright("Copyright ©Paythem  2019")]
[assembly: AssemblyTrademark("Paythem.Net™")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(true)]
[assembly: Guid("7dc01fa7-1e34-431c-bfa1-c5ee626f46ee")]
[assembly: AssemblyVersion("7.0.0.0")]
[assembly: AssemblyFileVersion("1.0.3.0")]
