﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace Paythem.Net
{
    public static class AESCTR
    {
        static readonly Encoding Enc = Encoding.Default;
        static readonly int BlockSize = 16;
        public static Response Encrypt(string StringToEncrypt, string Password, int Bits = 256)
        {
            Response R = new Response();
            if (string.IsNullOrWhiteSpace(StringToEncrypt))
                return R.SendError("Encryption string cannot be blank");
            if (string.IsNullOrWhiteSpace(Password))
                return R.SendError("Password cannot be blank");
            if (Bits != 256 && Bits != 192 && Bits != 128)
                return R.SendError("Invalid bit count: " + Bits + " (valid bits 128 / 192 / 256)");
            int[] PasswordArray;
            byte[] PasswordBytes;
            int NBytes = Bits / 8;
            PasswordArray = new int[NBytes];
            try
            {
                PasswordBytes = Enc.GetBytes(Password);
            }
            catch
            {
                return R.SendError("Could not retrieve bytes from password");
            }
            for (int I = 0; I < NBytes; I++)
            {
                try
                {
                    if (I < PasswordBytes.Length)
                        PasswordArray[I] = PasswordBytes[I];
                    else
                        PasswordArray[I] = 0;
                }
                catch
                {
                    return R.SendError("Password could not be translated to bytes");
                }
            }
            int[] Key;
            try
            {
                Key = Cipher(PasswordArray, KeyExpansion(PasswordArray));
            }
            catch
            {
                return R.SendError("Could not create cipher key");
            }
            int[] SecondKey = Key.Take(NBytes - 16).ToArray();
            if (SecondKey.Length > 0)
            {
                int[] KeyToBe = new int[32];
                List<int> L = new List<int>() { };
                L.AddRange(Key);
                L.AddRange(SecondKey);
                Key = L.ToArray();
            }
            int[] CounterBlock = new int[16];
            DateTime UnixEpoch = new DateTime(1970, 1, 1);
            TimeSpan span = DateTime.UtcNow - UnixEpoch;
            double Nonce = Math.Floor((double)1000 * span.TotalSeconds);
            double NonceMS = Nonce % 1000;
            double NonceSec = Math.Floor(Nonce / 1000);
            Random Rand = new Random();
            double NonceRnd = Math.Floor((double)Rand.Next(0, 0xffff));
            try
            {
                for (int I = 0; I < 2; I++)
                    CounterBlock[I] = Urs((int)NonceMS, I * 8) & 0xff;
                for (int I = 0; I < 2; I++)
                    CounterBlock[I + 2] = Urs((int)NonceRnd, I * 8) & 0xff;
                for (int I = 0; I < 4; I++)
                    CounterBlock[I + 4] = Urs((int)NonceSec, I * 8) & 0xff;
            }
            catch
            {
                return R.SendError("Counter block increment failure");
            }
            string CtrTxt = "";
            for (int I = 0; I < 8; I++)
                CtrTxt += Enc.GetString(new byte[] { Convert.ToByte(CounterBlock[I]) });
            List<int[]> KeySchedule = KeyExpansion(Key);
            double BlockCount = Math.Ceiling((double)StringToEncrypt.Length / BlockSize);
            if (BlockCount <= 0)
                return R.SendError("Block counter has no size");
            string CipherTxt = "";
            char[] Characters = StringToEncrypt.ToCharArray();
            for (int B = 0; B < BlockCount; B++)
            {
                for (int C = 0; C < 4; C++)
                {
                    CounterBlock[15 - C] = Urs(B, C * 8) & 0xff;
                }
                for (int C = 0; C < 4; C++)
                {
                    CounterBlock[15 - C - 4] = Urs(B / 0x100000000, C * 8);
                }
                int BlockLength = B < BlockCount - 1 ? BlockSize : (StringToEncrypt.Length - 1) % BlockSize + 1;
                int[] CipherCntr = Cipher(CounterBlock, KeySchedule);
                for (int I = 0; I < BlockLength; I++)
                {
                    int Count = (B * 16) + I;
                    try
                    {
                        char ThisCharacter = Characters[Count];
                        byte ThisByte = Convert.ToByte(Characters[Count]);
                        int Value = (int)ThisByte;
                        int Total = CipherCntr[I] ^ Value;
                        CipherTxt += Enc.GetString(new byte[] { Convert.ToByte(Total) });
                    }
                    catch
                    {
                        return R.SendError("Encryption characters could not be converted to bytes");
                    }
                }
            }
            string CipherText = CtrTxt + CipherTxt;
            try
            {
                string ToOutput = Convert.ToBase64String(Enc.GetBytes(CipherText));
                return R.SendSuccess(ToOutput);
            }
            catch
            {
                return R.SendError("Base64 encoding failed");
            }
        }

        public static Response Decrypt(string StringToDecrypt, string Password, int Bits = 256)
        {
            Response R = new Response();
            if (string.IsNullOrWhiteSpace(StringToDecrypt))
                return R.SendError("No decryption string provided");
            if (string.IsNullOrWhiteSpace(Password))
                return R.SendError("Password cannot be blank");
            if (Bits != 256 && Bits != 192 && Bits != 128)
                return R.SendError("Invalid bit count: " + Bits + " (valid bits 128 / 192 / 256)");
            try
            {
                StringToDecrypt = Enc.GetString(Convert.FromBase64String(StringToDecrypt));
            }
            catch
            {
                return R.SendError("Input was not a valid Base64 string");
            }
            int NBytes = Bits / 8;
            int[] PasswordArray = new int[NBytes];
            byte[] PasswordBytes = Enc.GetBytes(Password);
            for (int I = 0; I < NBytes; I++)
            {
                if (I < PasswordBytes.Length)
                    PasswordArray[I] = PasswordBytes[I];
                else
                    PasswordArray[I] = 0;
            }
            int[] Key = Cipher(PasswordArray, KeyExpansion(PasswordArray));
            int[] SecondKey = Key.Take(NBytes - 16).ToArray();
            if (SecondKey.Length > 0)
            {
                int[] KeyToBe = new int[32];
                List<int> L = new List<int>() { };
                L.AddRange(Key);
                L.AddRange(SecondKey);
                Key = L.ToArray();
            }
            string CtrTxt = StringToDecrypt.Substring(0, 8);
            char[] Characters = CtrTxt.ToCharArray();
            int[] CounterBlock = new int[BlockSize];
            for (int I = 0; I < 8; I++)
            {
                char ThisCharacter = Characters[I];
                byte[] BA = Enc.GetBytes(new char[] { ThisCharacter });
                byte ThisByte = Convert.ToByte(BA[0]);
                int Value = (int)ThisByte;
                CounterBlock[I] = Value;
            }
            List<int[]> KeySchedule = KeyExpansion(Key);
            double ToDe = (double)(StringToDecrypt.Length - 8) / BlockSize;
            int NBlocks = (int)Math.Ceiling(ToDe);
            if (NBlocks <= 0)
                return R.SendError("Block count of cipher invalid");
            string[] CipherText = new string[(int)NBlocks];
            string DecryptedText = "";
            for (int B = 0; B < NBlocks; B++)
            {
                int From = 8 + B * BlockSize;
                int ToCheck;
                if (StringToDecrypt.Length - From >= BlockSize)
                    ToCheck = BlockSize;
                else
                    ToCheck = StringToDecrypt.Length - From;
                CipherText[B] = StringToDecrypt.Substring(From, ToCheck);
            }
            for (int B = 0; B < NBlocks; B++)
            {
                for (int C = 0; C < 4; C++)
                {
                    int ToUse = 15 - C;
                    CounterBlock[15 - C] = Urs(B, C * 8) & 0xff;
                }
                for (int C = 0; C < 4; C++)
                {
                    CounterBlock[15 - C - 4] = Urs(B / 0x100000000, C * 8);
                }

                int[] CipherCntr = Cipher(CounterBlock, KeySchedule);
                string[] PlaintxtByte = new string[CipherText[B].Length];
                byte[] BA = Enc.GetBytes(CipherText[B]);
                char[] CChar = CipherText[B].ToCharArray();
                string[] PlainTextStringArray = new string[CipherText[B].Length];
                for (int I = 0; I < CipherText[B].Length; I++)
                {
                    try
                    {
                        int Count = (B * 16) + I;
                        byte C = BA[I];
                        int Value = CipherCntr[I] ^ C;
                        DecryptedText += Enc.GetString(new byte[] { Convert.ToByte(Value) });
                    }
                    catch
                    {
                        return R.SendError("Could not decipher encrypted bytes");
                    }
                }
            }
            return R.SendSuccess(DecryptedText);
        }

        private static int Urs(int A, int B)
        {
            int OldA = A;
            uint NewA = (uint)A;
            uint NewB = (uint)B;
            NewA &= 0xffffffff;
            NewB &= 0x1f;
            uint ACheck = NewA & 0x80000000;
            uint G = ACheck & NewB;
            int OA;
            int OB;
            if (G > 0)
            {
                OA = (int)(NewA >> 1) & 0x7fffffff;
                OB = (int)NewA >> (int)(NewB - 1);
            }
            else
                OA = (int)((int)NewA >> (int)NewB);
            return OA;
        }

        private static int Urs(long A, int B)
        {
            uint NewA = (uint)A;
            uint NewB = (uint)B;
            NewA &= 0xffffffff;
            NewB &= 0x1f;
            uint ACheck = NewA & 0x80000000;
            uint G = ACheck & NewB;
            int OA;
            int OB;
            if (G > 0)
            {
                OA = (int)(NewA >> 1) & 0x7fffffff;
                OB = (int)NewA >> (int)(NewB - 1);
            }
            else
                OA = (int)((int)NewA >> (int)NewB);
            return OA;
        }
        /* AES */
        private static readonly int[] SBox = {
        0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 0x30, 0x01, 0x67, 0x2b, 0xfe, 0xd7, 0xab, 0x76,
        0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0, 0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0,
        0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc, 0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15,
        0x04, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05, 0x9a, 0x07, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75,
        0x09, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0, 0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84,
        0x53, 0xd1, 0x00, 0xed, 0x20, 0xfc, 0xb1, 0x5b, 0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf,
        0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85, 0x45, 0xf9, 0x02, 0x7f, 0x50, 0x3c, 0x9f, 0xa8,
        0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5, 0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2,
        0xcd, 0x0c, 0x13, 0xec, 0x5f, 0x97, 0x44, 0x17, 0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73,
        0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88, 0x46, 0xee, 0xb8, 0x14, 0xde, 0x5e, 0x0b, 0xdb,
        0xe0, 0x32, 0x3a, 0x0a, 0x49, 0x06, 0x24, 0x5c, 0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79,
        0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9, 0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x08,
        0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6, 0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a,
        0x70, 0x3e, 0xb5, 0x66, 0x48, 0x03, 0xf6, 0x0e, 0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e,
        0xe1, 0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e, 0x94, 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf,
        0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68, 0x41, 0x99, 0x2d, 0x0f, 0xb0, 0x54, 0xbb, 0x16
        };
        private static readonly List<int[]> RCon = new List<int[]>() {
            new int[]{0x00, 0x00, 0x00, 0x00},
            new int[]{0x01, 0x00, 0x00, 0x00},
            new int[]{0x02, 0x00, 0x00, 0x00},
            new int[]{0x04, 0x00, 0x00, 0x00},
            new int[]{0x08, 0x00, 0x00, 0x00},
            new int[]{0x10, 0x00, 0x00, 0x00},
            new int[]{0x20, 0x00, 0x00, 0x00},
            new int[]{0x40, 0x00, 0x00, 0x00},
            new int[]{0x80, 0x00, 0x00, 0x00},
            new int[]{0x1B, 0x00, 0x00, 0x00},
            new int[]{0x36, 0x00, 0x00, 0x00}
        };
        private static int[] Cipher(int[] Input, List<int[]> Rounds)
        {
            int NB = 4;
            int Nr = Rounds.Count() / 4 - 1;
            List<int[]> State = new List<int[]>() { };
            for (int I = 0; I < 4 * 4; I++)
            {
                double D = I / 4;
                int T = (int)Math.Floor(D);
                try
                {
                    State[I % 4][T] = Input[I];
                }
                catch
                {
                    State.Insert(I % 4, new int[4]);
                    State[I % 4][T] = Input[I];
                }
            }
            State = AddRoundKey(State, Rounds, 0, NB);
            for (int Round = 1; Round < Nr; Round++)
            {
                State = SubBytes(State, NB);
                State = ShiftRows(State, NB);
                State = MixColumns(State, NB);
                State = AddRoundKey(State, Rounds, Round, NB);
            }
            State = SubBytes(State, NB);
            State = ShiftRows(State, NB);
            State = AddRoundKey(State, Rounds, Nr, NB);
            int[] Output = new int[4 * NB];
            for (int I = 0; I < 4 * NB; I++)
            {
                double D = I / 4;
                int C = (int)Math.Floor(D);
                Output[I] = State[I % 4][C];
            }
            return Output;
        }
        private static List<int[]> KeyExpansion(int[] Key)
        {
            int NB = 4;
            int NK = Key.Length / 4;
            int NR = NK + 6;
            int[] Temp = new int[] { 0, 0, 0, 0 };
            List<int[]> W = new List<int[]>() { };
            for (int I = 0; I < NK; I++)
            {
                int[] ThisArray = new int[] { Key[4 * I], Key[4 * I + 1], Key[4 * I + 2], Key[4 * I + 3] };
                W.Add(ThisArray);
            }
            for (int I = NK; I < (NB * (NR + 1)); I++)
            {
                W.Insert(I, new int[4]);
                for (int T = 0; T < 4; T++)
                {
                    int C1 = Temp[T];
                    int[] C2 = W[I - 1];
                    int C3 = W[I - 1][T];
                    Temp[T] = W[I - 1][T];
                }
                if (I % NK == 0)
                {
                    Temp = SubWord(RotWord(Temp));
                    for (int T = 0; T < 4; T++)
                    {
                        Temp[T] ^= RCon[I / NK][T];
                    }
                }
                else if (NK > 6 && I % NK == 4)
                    Temp = SubWord(Temp);
                for (int T = 0; T < 4; T++)
                    W[I][T] = W[I - NK][T] ^ Temp[T];
            }
            return W;
        }

        private static int[] SubWord(int[] Input)
        {
            for (int I = 0; I < 4; I++)
            {
                Input[I] = SBox[Input[I]];
            }
            return Input;
        }

        private static int[] RotWord(int[] W)
        {
            int Tmp = W[0];
            for (int I = 0; I < 3; I++)
            {
                W[I] = W[I + 1];
            }
            W[3] = Tmp;
            return W;
        }

        private static List<int[]> AddRoundKey(List<int[]> State, List<int[]> W, int Rnd, int NB)
        {
            for (int R = 0; R < 4; R++)
            {
                for (int C = 0; C < NB; C++)
                {
                    State[R][C] ^= W[Rnd * 4 + C][R];
                }
            }
            return State;
        }
        private static List<int[]> SubBytes(List<int[]> S, int NB)
        {
            for (int R = 0; R < 4; R++)
            {
                for (int C = 0; C < NB; C++)
                {
                    try
                    {
                        S[R][C] = SBox[S[R][C]];
                    }
                    catch
                    {
                        S.Insert(R, new int[4]);
                        S[R][C] = SBox[S[R][C]];
                    }
                }
            }
            return S;
        }
        private static List<int[]> ShiftRows(List<int[]> S, int NB)
        {
            int[] T = new int[] { 0, 0, 0, 0 };
            for (int R = 1; R < 4; R++)
            {
                for (int C = 0; C < 4; C++)
                    T[C] = S[R][(C + R) % NB];
                for (int C = 0; C < 4; C++)
                    S[R][C] = T[C];
            }
            return S;
        }

        private static List<int[]> MixColumns(List<int[]> S, int NB)
        {
            for (int C = 0; C < 4; C++)
            {
                int[] A = new int[] { 0, 0, 0, 0 };
                int[] B = new int[] { 0, 0, 0, 0 };
                for (int I = 0; I < 4; I++)
                {
                    A[I] = S[I][C];
                    int BA = S[I][C] & 0x80;
                    bool Type = true;
                    if (BA == 0)
                        Type = false;
                    B[I] = (Type) ? S[I][C] << 1 ^ 0x011b : S[I][C] << 1;
                }
                S[0][C] = B[0] ^ A[1] ^ B[1] ^ A[2] ^ A[3];
                S[1][C] = A[0] ^ B[1] ^ A[2] ^ B[2] ^ A[3];
                S[2][C] = A[0] ^ A[1] ^ B[2] ^ A[3] ^ B[3];
                S[3][C] = A[0] ^ B[0] ^ A[1] ^ A[2] ^ B[3];
            }
            return S;
        }
    }

    public class Response
    {
        public bool Success = true;
        public string Content;

        public override string ToString()
        {
            return Content;
        }

        internal Response SendSuccess(string ContentString)
        {
            Content = ContentString;
            return this;
        }

        internal Response SendError(string ErrorString, [CallerLineNumber] int line = 0)
        {
            Success = false;
            Content = "[" + line + "] " + ErrorString;
            return this;
        }
    }
}
